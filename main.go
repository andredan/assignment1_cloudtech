package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	//"time"
)

const version string = "V1"

const ApiGbif = "http://api.gbif.org/v1/"
const ApiRestcountries = "https://restcountries.eu/rest/v2/"

type Country struct {
	Code 		string 	`json:"alpha2Code"`
	Name 		string 	`json:"name"`
	CountryFlag string 	`json:"flag"`
	Species		[]string`json:"species"`
	SpeciesKey	[]string`json:"speciesKey"`
}

type Species struct {
	Key		string	`json:"key"`
	Kingdom string	`json:"kingdom"`
	Phylum	string	`json:"phylum"`
	Order 	string	`json:"order"`
	Family	string	`json:"family"`
	Genus 	string	`json:"genus"`
	SciName	string	`json:"scientificName"`
	CanName	string	`json:"canonicalName"`
	//Year	int		`json:"year"`
}

type Diag struct {
	Gbif 			string
	Restcountries 	string
	Version			string
	//Uptime
}
//func getApiStuff(url string){}

func handlerCountry(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")
	c := Country{}


	parts := strings.Split(r.URL.Path, "/")
	if len(parts[4]) != 2  {
		http.Error(w, "Invalid alpha2code, must be of lenght 2", http.StatusBadRequest)
		return
	}

	req,err := http.Get(ApiRestcountries + "alpha/" + parts[4])

	err = json.NewDecoder(req.Body).Decode(&c)
	if err != nil {
		fmt.Println("ErrorCountry1",err)
	}

	req,err = http.Get(ApiGbif + "occurrence/search?country=" + c.Code + "&limit=1")
	//http://api.gbif.org/v1/occurrence/search?country=DE&limit=1

	//Doesnt get decoded properly, i've tried to query species only without country information.
	err = json.NewDecoder(req.Body).Decode(&c)
	if err != nil {
		fmt.Println("ErrorCountry2",err)
	}
	country ,err := json.Marshal(&c)
	if err != nil {
		fmt.Println("ErrorCountry3",err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(country)
}

func handlerSpecies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")
	s := Species{}

	parts := strings.Split(r.URL.Path, "/")
	s.Key = parts[4]
	req,err := http.Get(ApiGbif + "species/" + s.Key)
	if parts[4] == ""  {
		http.Error(w, "Invalid URL", http.StatusBadRequest)
		return
	}
	//s.Key = parts[4]

	defer req.Body.Close()
	if err != nil {
		fmt.Println("ErrorSpecies1, help",err)
	}

	err = json.NewDecoder(req.Body).Decode(&s)
	if err != nil {
		fmt.Println("ErrorSpecies2",err)
	}

	species ,err := json.Marshal(&s)
	if err != nil {
		fmt.Println("ErrorSpecies3",err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(species)
}

func handlerDiag(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type","application/json")
	d := Diag{}

	req,err := http.Get(ApiGbif)
	if err != nil {
		fmt.Println("ErrorDiag1",err)
	}

	d.Gbif = http.StatusText(req.StatusCode)

	req,err = http.Get(ApiRestcountries)
	if err != nil {
		fmt.Println("ErrorDiag2",err)
	}
	d.Restcountries = http.StatusText(req.StatusCode)

	d.Version = version

	diag ,err := json.Marshal(&d)
	if err != nil {
		fmt.Println("ErrorDiag3",err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(diag)
}

func main (){
	port := os.Getenv("PORT")
	/*if port == "" {
		port = "8080"
	}*/
	http.HandleFunc("/conservation/v1/country/", handlerCountry )
	http.HandleFunc("/conservation/v1/species/", handlerSpecies)
	http.HandleFunc("/conservation/v1/diag/", handlerDiag)

	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":" + port, nil))
}

